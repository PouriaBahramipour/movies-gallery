
import { createStore } from 'vuex'

// Create a new store instance.
const store = createStore({
  state () {
    return {
      stateMovieClicked: ''
    }
  },
  mutations: {
    movieDetails(state, payload) {
        state.stateMovieClicked = payload
    },
  },
  actions: {
    movieDetails(state, payload) {
        state.commit('movieDetails', payload)
    },
  },
  getters: {
   getMovieDetails (state) {
      return state.stateMovieClicked
    }
  }
})

export default(store)