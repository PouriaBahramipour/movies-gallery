import axios from "axios";
const token ='f62f750b70a8ef11dad44670cfb6aa57';

export default axios.create({
    baseURL: 'https://api.themoviedb.org/3',
    headers: {
        Authorization: `Bearer ${token}`
    }
})