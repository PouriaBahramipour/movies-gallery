import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/Home.vue'
import MovieDetails from '../components/MovieDetails.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/MovieDetails',
    name: 'MovieDetails',
    component: MovieDetails
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
